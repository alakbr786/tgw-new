variable "aws_region" {
	default = "us-east-1"
}

variable "vpc_cidr_Production" {
	default = "10.20.0.0/16"
}

variable "vpc_cidr_Development" {
	default = "10.10.0.0/16"
}

variable "subnets_cidr_Production" {
	type = list
	default = ["10.20.1.0/24", "10.20.2.0/24"]
}

variable "subnets_cidr_Development" {
	type = list
	default = ["10.10.1.0/24", "10.10.2.0/24"]
}

variable "azs" {
	type = list
	default = ["us-east-1a", "us-east-1b"]
}

variable "auto_accept_shared_attachments" {
  description = "Whether resource attachment requests are automatically accepted"
  type        = string
  default     = "enable"

}

variable "amazon_side_asn" {
  description = "The Autonomous System Number (ASN) for the Amazon side of the gateway. By default the TGW is created with the current default Amazon ASN."
  type        = string
  default     = "65513" #just an example asn

}

variable "vpn_ecmp_support" {
  description = "Whether VPN Equal Cost Multipath Protocol support is enabled"
  type        = string
  default     = "enable"
}

variable "default_route_table_association" {
  description = "Whether resource attachments are automatically associated with the default association route table"
  type        = string
  default     = "enable"

}

variable "default_route_table_propagation" {
  description = "Whether resource attachments automatically propagate routes to the default propagation route table"
  type        = string
  default     = "enable"

}

variable "dns_support" {
  description = "Should be true to enable DNS support in the TGW"
  type        = string
  default     = "disable"

}

variable "transit_gateway_name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = "TGW"

}